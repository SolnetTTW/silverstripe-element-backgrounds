<?php

namespace Solnet\Elements\Backgrounds;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\Tests\MySQLDatabaseTest\Data;
use TractorCow\Colorpicker\Forms\ColorField;

class BackgroundMedia extends DataObject
{
    private static $table_name = 'BackgroundMedia';
    private static $singular_name = 'Background';
    private static $plural_name = 'Backgrounds';

    private static $db = [
        'BackgroundColor' => 'TractorCow\Colorpicker\Color',
        'TintColor' => 'TractorCow\Colorpicker\Color'
    ];

    private static $has_one = [
        'BackgroundImage' => 'SilverStripe\Assets\Image'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            'Root.Main',
            [
                LiteralField::create(
                    'Tips',
                    _t('ElementsBackgrounds.BackgroundMedia_Tips', '<p class="message warning">Background image if provided will take affect over background colour.</p>')
                ),
                ColorField::create(
                    'TintColor',
                    _t('ElementsBackgrounds.BackgroundMedia_TintColour_Title', 'Tint Colour')
                ),
                ColorField::create(
                    'BackgroundColor',
                    _t('ElementsBackgrounds.BackgroundMedia_BackgroundColour_Title', 'Background Colour')
                ),
                $imageUpload = UploadField::create(
                    'BackgroundImage',
                    _t('ElementsBackgrounds.BackgroundMedia_BackgroundImage_Title', 'Background Image')
                )
            ]
        );

        $imageUpload->getValidator()->setAllowedExtensions(array('jpg', 'svg', 'png', 'jpeg'));

        return $fields;
    }
}
