<?php

namespace Solnet\Elements\Backgrounds;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;

class VideoBackground extends BackgroundMedia
{
    private static $table_name = 'VideoBackground';
    private static $singular_name = 'Background';
    private static $plural_name = 'Backgrounds';

    private static $db = [
        'ExternalVideoLink' => 'Text',
        'PlayInPopup' => 'Boolean'
    ];

    private static $has_one = [
        'BackgroundVideo' => 'SilverStripe\Assets\File'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Tips');

        $fields->addFieldsToTab(
            'Root.Main',
            [
                LiteralField::create(
                    'Tips',
                    _t(
                        'ElementsBackgrounds.VideoBackground_Tips',
                        '<p class="message warning">Background video if provided will take affect over background image and in turn will take affect over background colour.<br>Internal files takes priority over external sources.</p>'
                    )
                ),
                $videoUpload = UploadField::create(
                    'BackgroundVideo',
                    _t('ElementsBackgrounds.VideoBackground_BackgroundVideo_Title', 'Background Video')
                ),
                TextField::create(
                    'ExternalVideoLink',
                    _t('ElementsBackgrounds.VideoBackground_ExternalVideoLink_Title', 'External Video Link')
                ),
                CheckboxField::create(
                    'PlayInPopup',
                    _t('ElementsBackgrounds.VideoBackground_PlayInPopup_Title', 'Play video in a popup?')
                )
            ]
        );

        $videoUpload->getValidator()->setAllowedExtensions(array('mp4', 'ogg'));

        return $fields;
    }

    public function VideoType()
    {
        $type = '';
        $url = parse_url($this->ExternalVideoLink);
        if (isset($url['host']) && isset($url['path'])) {
            if (strpos($url['host'], 'youtu') !== false) {
                $type = 'youtube';
            } elseif (strpos($url['host'], 'vimeo') !== false) {
                $type = 'vimeo';
            }
        }

        return $type;
    }

    public function VideoID()
    {
        $url = parse_url($this->ExternalVideoLink);
        if (isset($url['host']) && isset($url['path'])) {
            $query = $url['path'];
            if (substr($url['path'], 0, 1) === DIRECTORY_SEPARATOR) {
                $query = substr($query, 1);
            }
            return $query;
        }
    }
}
